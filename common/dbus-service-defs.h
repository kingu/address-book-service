/*
 * Copyright 2013 Canonical Ltd.
 *
 * This file is part of contact-service-app.
 *
 * contact-service-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DBUS_SERVICE_DEFS_H__
#define __DBUS_SERVICE_DEFS_H__

#define ALTERNATIVE_LPIM_SERVICE_NAME       "LOMIRI_PIM_SERVICE_NAME"
#define LPIM_SERVICE_NAME                   "com.lomiri.pim"
#define LPIM_ADDRESSBOOK_OBJECT_PATH        "/com/lomiri/pim/AddressBook"
#define LPIM_ADDRESSBOOK_IFACE_NAME         "com.lomiri.pim.AddressBook"
#define LPIM_ADDRESSBOOK_VIEW_OBJECT_PATH   "/com/lomiri/pim/AddressBookView"
#define LPIM_ADDRESSBOOK_VIEW_IFACE_NAME    "com.lomiri.pim.AddressBookView"

//Updater
#define LPIM_UPDATE_SERVICE_NAME              "com.lomiri.pim.updater"
#define LPIM_UPDATE_OBJECT_PATH               "/com/lomiri/pim/Updater"
#define LPIM_UPDATE_OBJECT_IFACE_NAME         "com.lomiri.pim.Updater"

#endif
